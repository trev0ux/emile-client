#include <QCoreApplication>
#include <QDebug>
#include <QFile>

#include <QXmlQuery>
#include <tidy.h>
#include <tidybuffio.h>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QFile file("<your-html-file>");
    file.open(QIODevice::ReadOnly);
    QString payload = file.readAll();

    TidyDoc tdoc = tidyCreate();
    tidyOptSetBool(tdoc, TidyXmlOut, yes);
    tidyOptSetBool(tdoc, TidyQuiet, yes);
    tidyOptSetBool(tdoc, TidyNumEntities, yes);
    tidyOptSetBool(tdoc, TidyUseCustomTags, yes);
    tidyOptSetBool(tdoc, TidyShowWarnings, no);

    tidyParseString(tdoc, payload.toUtf8());
    tidyCleanAndRepair(tdoc);
    TidyBuffer output = {nullptr, nullptr, 0, 0, 0};
    tidySaveBuffer(tdoc, &output);

    payload = QString::fromUtf8(reinterpret_cast<char*>(output.bp));

    QXmlQuery xmlQuery;
    xmlQuery.setFocus(payload);
    xmlQuery.setQuery("<your-xpath-query>");

    QStringList list;
    xmlQuery.evaluateTo(&list);
    qDebug() << list;

    return a.exec();
}
